#LyX 2.3 created this file. For more info see http://www.lyx.org/
\lyxformat 544
\begin_document
\begin_header
\save_transient_properties true
\origin unavailable
\textclass book
\begin_preamble
\usepackage[utf8]{inputenc}
\usepackage[spanish]{babel}
\usepackage{a4wide}
\usepackage[graf, intHoriz, sinLU, showDirectores, showCoDirectores]{caratula}
\usepackage{hyperref}

\let\oldhat\hat
\renewcommand{\vec}[1]{\mathbf{#1}}
\renewcommand{\hat}[1]{\oldhat{\mathbf{#1}}}

\newcommand{\abs}[1]{\lvert#1\rvert}
\newcommand{\norm}[1]{\lVert#1\rVert}
\end_preamble
\use_default_options true
\begin_modules
theorems-ams
eqs-within-sections
figs-within-sections
\end_modules
\maintain_unincluded_children false
\language spanish
\language_package default
\inputencoding utf8
\fontencoding global
\font_roman "default" "default"
\font_sans "default" "default"
\font_typewriter "default" "default"
\font_math "auto" "auto"
\font_default_family default
\use_non_tex_fonts false
\font_sc false
\font_osf false
\font_sf_scale 100 100
\font_tt_scale 100 100
\use_microtype false
\use_dash_ligatures true
\graphics pdftex
\default_output_format default
\output_sync 0
\bibtex_command default
\index_command default
\float_placement tph
\paperfontsize 10
\spacing single
\use_hyperref false
\papersize a4paper
\use_geometry false
\use_package amsmath 1
\use_package amssymb 1
\use_package cancel 1
\use_package esint 1
\use_package mathdots 1
\use_package mathtools 1
\use_package mhchem 1
\use_package stackrel 1
\use_package stmaryrd 1
\use_package undertilde 1
\cite_engine basic
\cite_engine_type default
\biblio_style plain
\use_bibtopic false
\use_indices false
\paperorientation portrait
\suppress_date false
\justification true
\use_refstyle 1
\use_minted 0
\index Index
\shortcut idx
\color #008000
\end_index
\secnumdepth 3
\tocdepth 3
\paragraph_separation skip
\defskip smallskip
\is_math_indent 0
\math_numbering_side default
\quotes_style english
\dynamic_quotes 0
\papercolumns 1
\papersides 2
\paperpagestyle plain
\tracking_changes false
\output_changes false
\html_math_output 0
\html_css_as_file 0
\html_be_strict false
\end_header

\begin_body

\begin_layout Chapter
\begin_inset CommandInset label
LatexCommand label
name "chap:Propuesta"

\end_inset

Propuesta general
\begin_inset CommandInset label
LatexCommand label
name "chap:Propuesta-general"

\end_inset


\end_layout

\begin_layout Standard
\begin_inset ERT
status open

\begin_layout Plain Layout


\backslash
def
\backslash
cpp{C{}
\backslash
texttt{++}}
\end_layout

\end_inset


\end_layout

\begin_layout Standard
El trabajo realizado en este proyecto se puede separar en dos partes: uno
 referente al análisis y evaluación de características visuales y otra que
 concierne a la implementación y evaluación en hardware de un sistema SLAM
 con las modificaciones propuestas.
 En este capítulo se comienza repasando las tecnologías utilizadas durante
 el desarrollo del proyecto y luego se describen brevemente dichas partes.
\end_layout

\begin_layout Section
Tecnologías utilizadas
\end_layout

\begin_layout Standard
Muchos sistemas SLAM, incluido el seleccionado para realizar las evaluaciones
 de este proyecto (S-PTAM), están implementados en lenguaje 
\begin_inset ERT
status open

\begin_layout Plain Layout


\backslash
cpp{}
\end_layout

\end_inset

 dado que requieren gran eficiencia y, además, hacen uso de la librería
 OpenCV, cuyas funciones están optimizadas para dicho lenguaje.
 A su vez, se realizó un software de análisis de comparación de distintos
 detectores, descriptores y matchers escrito en 
\begin_inset ERT
status open

\begin_layout Plain Layout


\backslash
cpp{}
\end_layout

\end_inset

, el cual también utiliza OpenCV, para que sus resultados sean consistentes
 con todo el desarrollo del proyecto.
\end_layout

\begin_layout Standard
OpenCV
\begin_inset Foot
status open

\begin_layout Plain Layout
\begin_inset ERT
status open

\begin_layout Plain Layout


\backslash
url{https://opencv.org/}
\end_layout

\end_inset


\end_layout

\end_inset

 es una librería de visión por computadora creada por Intel y está disponible
 para múltiples plataformas como Windows, Linux, Mac y Android.
 Además, cuenta con soporte para diferentes lenguajes como: Python, Java,
 C/
\begin_inset ERT
status open

\begin_layout Plain Layout


\backslash
cpp{}
\end_layout

\end_inset

 y .Net.
 Cabe destacar que es de código abierto y tiene una amplia comunidad que
 aporta a los diferentes módulos que la conforman.
 Las aplicaciones de esta librería incluyen análisis y procesamiento de
 imágenes o videos, robótica, seguimiento y detección de objetos, reconocimiento
 facial, análisis de formas, reconstrucción 3D, realidad aumentada, entre
 muchas otras.
 Se utiliza OpenCV en su versión 3.4.1, y también los módulos de contribución
 realizados por la comunidad OpenCV 
\shape italic
contrib
\shape default
, con especial énfasis en los módulos 
\shape italic
features2d
\shape default
 y 
\shape italic
xfeatures2d
\shape default
 (además de algunos módulos que conforman el núcleo de OpenCV).
 En estos últimos módulos se encuentran disponibles los códigos de algunos
 detectores, descriptores y matchers utilizados en este proyecto.
 Las características visuales agregadas en este proyecto se compatibilizan
 con OpenCV, de modo que incluyendo el módulo xfeatures2d estén disponibles
 para su utilización.
\end_layout

\begin_layout Standard
Asimismo, S-PTAM utiliza ROS
\begin_inset Foot
status open

\begin_layout Plain Layout
\begin_inset ERT
status open

\begin_layout Plain Layout


\backslash
url{http://www.ros.org/}
\end_layout

\end_inset


\end_layout

\end_inset

 (
\shape italic
Robot Operating System
\shape default
) para algunas de sus funcionalidades.
 ROS consiste en librerías y herramientas para ayudar a los desarrolladores
 de software a crear aplicaciones para robots.
 Provee abstracción de hardware, controladores de dispositivos, librerías,
 herramientas de visualización, comunicación por mensajes, administración
 de paquetes, entre otras utilidades.
 Cabe destacar que ROS también es de código abierto y tiene una comunidad
 de contribuyentes muy activa.
 ROS permite observar las distintas variables del sistema SLAM en tiempo
 de ejecución.
 Por ejemplo, se puede visualizar la trayectoria que se va estimando, la
 creación del mapa de puntos, la secuencia de imágenes, entre otras.
 Para que se pueda procesar adecuadamente una secuencia de imágenes, ROS
 utiliza un formato denominado 
\shape italic
rosbag 
\shape default
que contiene información sobre la calibración de la cámara y los instantes
 de tiempo en que fueron capturados los distintos fotogramas
\shape italic
.
 
\shape default
Dado que en este proyecto se grabaron secuencias propias, se debieron adaptar
 las mismas a dicho formato.
\end_layout

\begin_layout Standard
\begin_inset Note Note
status open

\begin_layout Plain Layout
Por último, se utilizó Python para realizar gráficas de trayectorias calculadas,
 errores cometidos, tiempos consumidos, entre otras, que se obtienen al
 aplicar el sistema SLAM sobre una secuencia de un dataset.
\end_layout

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Note Note
status open

\begin_layout Plain Layout
Los lenguajes y librerías empleados para realizar las distintas tareas de
 este proyecto fueron:
\end_layout

\begin_layout Subsubsection*
\begin_inset ERT
status open

\begin_layout Plain Layout


\backslash
cpp{}
\end_layout

\end_inset


\end_layout

\begin_layout Plain Layout
Como sabemos, 
\begin_inset ERT
status open

\begin_layout Plain Layout


\backslash
cpp{}
\end_layout

\end_inset

 es un lenguaje de programación que extiende a C, agregando mecanismos que
 permiten la manipulación de objetos, es decir, es un lenguaje de programación
 orientado a objetos.
 Dado que el sistema SLAM seleccionado (y gran cantidad de otros sistemas)
 está escrito en este lenguaje, todos los agregados y adaptaciones realizadas
 en el proyecto también lo están.
 Una de las razones por las que se utiliza 
\begin_inset ERT
status open

\begin_layout Plain Layout


\backslash
cpp{}
\end_layout

\end_inset

 es que las funciones de la librería OpenCV están muy optimizadas para este
 lenguaje, con tiempos de procesamiento mucho más rápidos respecto a otros
 lenguajes compatibles (por ejemplo Python y Java).
 Esto es de gran importancia si se quiere que los sistemas SLAM se ejecuten
 en tiempo real.
 Mas aún, en nuestro caso, donde se busca una ejecución de este tipo en
 un dispositivo sin demasiada capacidad de cómputo, utilizar 
\begin_inset ERT
status open

\begin_layout Plain Layout


\backslash
cpp{}
\end_layout

\end_inset

 es lo adecuado.
 Por otro lado, también se realizó un software de prueba de distintos detectores
 / descriptores y matchers escrito en 
\begin_inset ERT
status open

\begin_layout Plain Layout


\backslash
cpp{}
\end_layout

\end_inset

, de modo que los experimentos desarrollados con el mismo sean consistentes
 con todo el desarrollo del proyecto.
\end_layout

\begin_layout Subsubsection*
OpenCV 3
\end_layout

\begin_layout Plain Layout
OpenCV es una librería de visión por computadora creada por Intel y está
 disponible para múltiples plataformas como Windows, Linux, Mac y Android.
 Además, cuenta con soporte para diferentes lenguajes como: Python, Java,
 C/
\begin_inset ERT
status open

\begin_layout Plain Layout


\backslash
cpp{}
\end_layout

\end_inset

 y .Net.
 Cabe destacar que es de código abierto y tiene una amplia comunidad que
 aporta a los diferentes módulos que la conforman.
 Las aplicaciones de esta librería incluyen análisis y procesamiento de
 imágenes o vídeos, robótica, seguimiento y detección de objetos, reconocimiento
 facial, análisis de formas, reconstrucción 3D, realidad aumentada, entre
 muchas otras.
 Utilizamos OpenCV 3 en su versión 3.4.1, y también los módulos de contribución
 realizados por la comunidad OpenCV contrib.
 Específicamente nos concentramos en los módulos features2d y xfeatures2d
 (además de algunos módulos que conforman el núcleo de OpenCV).
 En dichos módulos se encuentran disponibles los códigos de los distintos
 detectores, descriptores y matchers.
 Los sistemas SLAM basados en características visuales hacen uso de los
 mismos en su fase de extracción, por lo que generalmente se incluyen dichos
 módulos en su código.
 Las características visuales agregadas en este proyecto se compatibilizan
 con OpenCV, de modo que incluyendo el módulo xfeatures2d están disponibles
 para su utilización.
 Los agregados y compatibilización de dichas características visuales fueron
 subidos al repositorio de la última versión no estable de OpenCV.
 Si los cambios fueran aprobados, podrían aparecer en las últimas versiones
 estables de la librería.
\end_layout

\begin_layout Subsubsection*
Python 
\end_layout

\begin_layout Plain Layout
Python es un lenguaje de programación multiparadigma (soporta orientación
 a objetos, programación imperativa y programación funcional) y multiplataforma.
 Sus creadores hicieron hincapié en lograr un lenguaje con una sintaxis
 que favorezca un código legible.
 En nuestro caso, hemos utilizado este lenguaje para realizar gráficas y
 obtener datos de las distintas variables que se obtienen de un 
\shape italic
logfile
\shape default
 al aplicar S-PTAM sobre una secuencia de un dataset.
\begin_inset Note Note
status open

\begin_layout Plain Layout
Esto se logra gracias a la librería matplotlib, que permite la generación
 de gráficos fácilmente a partir de datos contenidos en listas o arreglos
 (en nuestro caso los datos estarán dentro del logfile).
 Cabe destacar que esta librería es compatible para utilizarse con la extensión
 matemática de Python, numpy, facilitando su uso.
 
\end_layout

\end_inset


\end_layout

\begin_layout Subsubsection*
ROS (Robot Operating System)
\end_layout

\begin_layout Plain Layout
ROS provee librerías y herramientas para ayudar a los desarrolladores de
 software a crear aplicaciones para robots.
 Provee abstracción de hardware, controladores de dispositivos, librerías,
 herramientas de visualización, comunicación por mensajes, administración
 de paquetes, entre otras utilidades.
 Cabe destacar que ROS también es de código abierto y tiene una comunidad
 de contribuyentes.
 
\end_layout

\begin_layout Plain Layout
ROS nos permite observar las distintas variables del sistema SLAM en tiempo
 de ejecución.
 Por ejemplo, podemos visualizar la trayectoria que se va calculando, la
 creación del mapa de puntos, la secuencia de imágenes, entre otras.
 
\end_layout

\end_inset


\end_layout

\begin_layout Section

\series bold
Análisis de características visuales
\begin_inset CommandInset label
LatexCommand label
name "sec:Análisis-de-características"

\end_inset


\end_layout

\begin_layout Standard
En la Figura 
\begin_inset CommandInset ref
LatexCommand ref
reference "fig:diag-features"
plural "false"
caps "false"
noprefix "false"

\end_inset

 se muestra el proceso de análisis de características visuales locales.
\end_layout

\begin_layout Standard
\begin_inset Float figure
placement tbh
wide false
sideways false
status open

\begin_layout Plain Layout
\align center
\begin_inset Graphics
	filename imagenes/desarrollo_features.svg
	lyxscale 50
	width 90text%

\end_inset


\end_layout

\begin_layout Plain Layout
\begin_inset Caption Standard

\begin_layout Plain Layout
Diagrama de flujo de fase de análisis de características visuales.
 
\begin_inset CommandInset label
LatexCommand label
name "fig:diag-features"

\end_inset


\end_layout

\end_inset


\end_layout

\end_inset


\end_layout

\begin_layout Standard
En esta parte del desarrollo del proyecto se profundiza sobre las característica
s visuales con el objetivo de mejorar la eficiencia/precisión de sistemas
 SLAM basados en las mismas.
 En un primer paso, se estudia el estado del arte (recurriendo a papers,
 libros y repositorios) en lo que se refiere a esta temática.
 Tras dicho estudio se encuentra que también es importante la evaluación
 de algoritmos de matching y un algoritmo que permita obtener una correcta
 distribución y cantidad de puntos de interés, como lo es ANMS.
 A partir de esto, se seleccionan nuevos detectores/descriptores, los cuales
 se adaptan a la librería OpenCV para poder utilizarse.
 Dicha selección se realiza priorizando un bajo costo computacional sin
 perder de vista que posteriormente se emplea un dispositivo de bajo poder
 de cómputo.
\end_layout

\begin_layout Standard
El siguiente paso es crear un software que permite evaluar y realizar un
 análisis comparativo de las características de diferentes detectores, descripto
res y matchers.
 Se evalúan tanto aquellos que ya están implementados en OpenCV como aquellos
 que no.
 A partir de este análisis se determinan los detectores, descriptores y
 matchers que pueden resultar útiles en sistemas SLAM, especialmente en
 dispositivos de bajo poder de cómputo.
 Para la evaluación de estos últimos en SLAM, se selecciona el sistema S-PTAM
 dado que es un sistema basado en características visuales locales, es del
 estado del arte y de código abierto.
 Aquí se realiza un análisis exhaustivo de distintas combinaciones detector/desc
riptor, matchers y el algoritmo ANMS.
 Se observa el impacto que estos tienen en tiempos de cómputo, errores,
 trayectorias estimadas, entre otras características relevantes que produce
 el sistema SLAM
\begin_inset Note Note
status open

\begin_layout Plain Layout
 (utilizando scripts en Python, capaces de interpretar la información contenida
 en los 
\shape italic
logfiles
\shape default
 que se obtienen como salida)
\end_layout

\end_inset

.
 Para dicha evaluación se requiere utilizar datasets de uso común en sistemas
 de visión estéreo, como KITTI Vision Benchmark Suite 
\begin_inset CommandInset citation
LatexCommand cite
key "geiger2013vision"
literal "false"

\end_inset

, EuRoC MAV 
\begin_inset CommandInset citation
LatexCommand cite
key "euroc2016"
literal "false"

\end_inset

, MIT Stata Center 
\begin_inset CommandInset citation
LatexCommand cite
key "mit2013"
literal "false"

\end_inset

, Level 7 S-Block 
\begin_inset CommandInset citation
LatexCommand cite
key "level72012"
literal "false"

\end_inset

, Institut Pascal Dataset 
\begin_inset CommandInset citation
LatexCommand cite
key "ipds2013"
literal "false"

\end_inset

.
 En este punto, cabe destacar que también se adecúa y se crean scripts de
 análisis para un dataset particular (ver Institut Pascal Dataset en la
 sección 
\begin_inset CommandInset ref
LatexCommand ref
reference "subsec:Datasets"
plural "false"
caps "false"
noprefix "false"

\end_inset

) para que pueda analizarse el desempeño de S-PTAM sobre éste.
 Esto permite abordar posteriormente el desarrollo de secuencias propias.
\end_layout

\begin_layout Standard
Finalmente, con todos los datos obtenidos de la evaluación anterior se obtiene
 una adecuada configuración (conjunto detector/descriptor, matcher y ANMS)
 para utilizarse en el dispositivo de bajo poder de cómputo.
 
\end_layout

\begin_layout Standard
\begin_inset Note Note
status open

\begin_layout Plain Layout
El proyecto tiene varias ramas que comienzan casi independientemente pero
 que confluyen sobre el final, al momento de testear las distintas configuracion
es sobre el sistema SLAM seleccionado (S-PTAM), esto se esquematiza en la
 Figura 
\begin_inset CommandInset ref
LatexCommand ref
reference "fig:diag-features"
plural "false"
caps "false"
noprefix "false"

\end_inset

 que se explicará a continuación.
\end_layout

\end_inset


\end_layout

\begin_layout Section
Implementación en hardware de bajo costo
\end_layout

\begin_layout Standard
En la Figura 
\begin_inset CommandInset ref
LatexCommand ref
reference "fig:diag-hardware"
plural "false"
caps "false"
noprefix "false"

\end_inset

 se muestra el proceso de implementación en hardware.
\end_layout

\begin_layout Standard
\align center
\begin_inset Float figure
placement h
wide false
sideways false
status open

\begin_layout Plain Layout
\align center
\begin_inset Graphics
	filename imagenes/desarrollo_hardware.svg
	lyxscale 50
	width 90text%

\end_inset


\end_layout

\begin_layout Plain Layout
\begin_inset Caption Standard

\begin_layout Plain Layout
Diagrama de flujo de fase de implementación en hardware.
 
\begin_inset CommandInset label
LatexCommand label
name "fig:diag-hardware"

\end_inset


\end_layout

\end_inset


\end_layout

\end_inset


\end_layout

\begin_layout Standard
Se comienza esta parte del desarrollo del proyecto con la selección de hardware
 adecuado, tanto del dispositivo de baja capacidad de procesamiento, como
 de la cámara estéreo.
 Con respecto a esta última, se implementan algunas funcionalidades tomando
 como base software pre-existente de otras cámaras, de modo de tener programas
 que nos permitan visualizar las imágenes, grabar video, calibrar la cámara,
 entre otras.
 Tras la calibración de la cámara y observando una adecuada eliminación
 de distorsión radial y rectificación, se está en condiciones para el grabado
 de secuencias propias.
\end_layout

\begin_layout Standard
\begin_inset Note Note
status open

\begin_layout Plain Layout
Anteriormente, mencionamos que se incorporó un dataset a los ya probados
 en S-PTAM (Institut Pascal).
 Aquí se profundizó sobre todo lo necesario para que el mismo pueda correrse
 en ROS y pueda ser probado con el sistema SLAM.
 Para esto, se investigó por ejemplo, sobre parámetros de cámara y calibración
 y la obtención de un formato adecuado de los mismos, 
\begin_inset Note Note
status open

\begin_layout Plain Layout
sobre cual es el significado qué indica cada parámetro de la 
\shape italic
camera matrix
\shape default
,
\end_layout

\end_inset

 cómo crear un archivo rosbag y cómo crear un archivo 
\shape italic
launch
\shape default
 para ROS, de forma que tome la información contenida en el rosbag y elimine
 la distorsión radial y rectifique las imágenes, entre otras.
\end_layout

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Note Note
status open

\begin_layout Plain Layout
Con el proceso aprendido en la etapa anterior, se incurrió a grabar nuestras
\end_layout

\end_inset

Las grabaciones de secuencias son realizadas en lugares comunes de estudiantes
 de Ingeniería Electrónica, tales como el Nuevo Edificio de Ingeniería Civil
 y Electrónica y el Instituto de Mecánica Aplicada y Estructuras (IMAE).
 Tras dar el formato adecuado que debe tener la información obtenida a partir
 de las grabaciones, se crean los archivos rosbag correspondientes a cada
 secuencia, quedando aptos para ser evaluados en sistemas SLAM.
\end_layout

\begin_layout Standard
Por otro lado, respecto al dispositivo onboard, se implementa el sistema
 SLAM con la configuración adecuada de modo de tener un procesamiento mínimo.
 Aquí, se utiliza la combinación detector/descriptor, matcher y ANMS obtenidos
 de la etapa anterior (ver Sección 
\begin_inset CommandInset ref
LatexCommand ref
reference "sec:Análisis-de-características"
plural "false"
caps "false"
noprefix "false"

\end_inset

).
 
\end_layout

\begin_layout Standard
Finalmente, se evaluan tiempos de cómputo, precisión, trayectorias estimadas,
 que se obtienen al correr el sistema SLAM en el dispositivo de bajo poder
 de cómputo, sobre las secuencias propias y de datasets como KITTI, y se
 concluye sobre la utilidad del proyecto.
\end_layout

\begin_layout Standard
\begin_inset Note Note
status open

\begin_layout Plain Layout
En un primer paso, se estudio el estado del arte (recurriendo a papers,
 libros y repositorios) en lo que se refiere a características visuales
 locales.
 Esto permitió una correcta selección de nuevos detectores/descriptores
 para su posterior adaptación a la librería de OpenCV.
 Una vez logrado esto, se hizo un análisis comparativo de cada conjunto
 detector/descriptor con un software realizado para dicho fin
\begin_inset Note Note
status open

\begin_layout Plain Layout
para poder compararlos con los ya implementados, en cuanto a los tiempos
 de procesamiento, repetibilidad, distribución de los puntos, etc
\end_layout

\end_inset

.
 En la búsqueda, se priorizó un bajo costo computacional de modo de tener
 un funcionamiento en tiempo real (o cercano al mismo), siendo esto aun
 más importante para la implementación en un dispositivo dedicado.
 Respecto a este requerimiento, se encontró que los detectores de esquinas
 y los descriptores de tipo binario son los más adecuados, realizando sus
 tareas en pocos milisegundos (de modo que se buscaron features primordialmente
 de este tipo) Cabe destacar, que también se analizaron varios matchers
 recientes, los cuales también fueron puestos a prueba.
\end_layout

\begin_layout Plain Layout
Luego, tras la realización de un software de prueba de detectores, descriptores
 y matchers, se realizó un análisis comparativo de ellos.
 Se pusieron a prueba tanto aquellos que ya estaban implementados en OpenCV
 como los recientes.
 Aquí, se probaron características como velocidad de detección, descripción
 y matching, cantidad de puntos detectados, cantidad de puntos encontrados
 y distinguidos (emparejados) en ambas imágenes, comportamiento ante rotaciones
 de cámara, etc.
 A partir de esto, se seleccionaron aquellos que presentaron un mejor desempeño
 y se distinguió entre los adecuados a utilizarse en computadoras de altos
 recursos y aquellos adecuados para dispositivos dedicados.
 Para dicha distinción se tuvo en cuenta especialmente los tiempos de computo.
\end_layout

\begin_layout Plain Layout
La próxima etapa fueron los agregados de código y adaptaciones necesarias
 para que S-PTAM funcione con los nuevos features, de modo que se puedan
 utilizar sin problemas.
 A partir de esto, con las distintas selecciones detector / descriptor se
 prueba una primera vez el desempeño de S-PTAM, analizando superficialemente
 trayectorias realizadas , tiempos, errores, etc.
 Dicho análisis se hace con la utilización de scripts de Python, capaces
 de interpretar la información contenida en los logfiles que produce S-PTAM
 como salida.
\end_layout

\begin_layout Plain Layout
Teniendo en cuenta lo anterior, se saca conclusiones sobre los conjuntos
 detector / descriptor y matcher más adecuados, especialmente para la utilizació
n en el dispositivo dedicado.
 Finalmente se hace dicha implementación y se prueba su desempeño.
 En este punto, se optimizan los recursos de modo de obtener un funcionamiento
 en tiempo real
\begin_inset Note Note
status open

\begin_layout Plain Layout
 (cercano??)
\end_layout

\end_inset

, con una precisión aceptable.
\end_layout

\begin_layout Plain Layout
Al mismo tiempo, profundizando sobre el formato y los parámetros necesarios
 para poder evaluar un nuevo dataset, decidimos incorporar alguno que no
 haya sido implementado para poder corroborar nuestras incorporaciones sobre
 nuevos ambientes.
 Para esto, tuvimos que aprender qué indica cada parámetro de la camera
 matrix, cómo armar un rosbag (para que pueda ser leído por S-PTAM) y cómo
 crear un launch para ROS, de forma que tome la información contenida en
 el rosbag y elimine la distorsión y rectifique las imágenes.
\end_layout

\begin_layout Plain Layout
Con lo aprendido en la etapa anterior, incurrimos a grabar nuestra propia
 secuencia dentro de la facultad con una nueva cámara, debiendo profundizar
 sobre los modelos de cámaras, calibración, el nuevo hardware, etc.
 para poder probarlo sobre S-PTAM.
 Para ello, tomamos como base software pre-existente para otras cámaras,
 de modo de tener programas que nos permitan: visualizar las imágenes, grabar
 video, calibrar la cámara y obtener todo ello en el formato correcto para
 el posterior armado del rosbag, esto se hizo, nuevamente, en 
\begin_inset ERT
status open

\begin_layout Plain Layout


\backslash
cpp{}
\end_layout

\end_inset

 sobre OpenCV.
\end_layout

\begin_layout Plain Layout
Finalmente, con todos los datos recolectados procedimos a la evaluación
 de los distintos datasets, tanto sobre nuestras laptops como sobre la odroid
 y comprobamos si alcanzamos nuestros objetivos.
\end_layout

\end_inset


\end_layout

\end_body
\end_document
