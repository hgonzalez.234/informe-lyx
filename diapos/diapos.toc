\beamer@endinputifotherversion {3.36pt}
\select@language {spanish}
\beamer@sectionintoc {1}{Motivaci\IeC {\'o}n}{2}{0}{1}
\beamer@sectionintoc {2}{Introducci\IeC {\'o}n Te\IeC {\'o}rica}{3}{0}{2}
\beamer@subsectionintoc {2}{1}{SLAM}{3}{0}{2}
\beamer@subsectionintoc {2}{2}{Caracter\IeC {\'\i }sticas Visuales Locales}{5}{0}{2}
\beamer@sectionintoc {3}{An\IeC {\'a}lisis}{8}{0}{3}
\beamer@subsectionintoc {3}{1}{Software de Comparaci\IeC {\'o}n}{8}{0}{3}
\beamer@subsectionintoc {3}{2}{S-PTAM}{11}{0}{3}
\beamer@sectionintoc {4}{Hardware}{15}{0}{4}
\beamer@subsectionintoc {4}{1}{Set-up}{15}{0}{4}
\beamer@subsectionintoc {4}{2}{Resultados}{18}{0}{4}
\beamer@sectionintoc {5}{Conclusiones}{20}{0}{5}
