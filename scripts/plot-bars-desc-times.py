import numpy as np
import matplotlib.pyplot as plt
from utils.colors import colors

# data to plot
n_groups = 8

times = (2.6, 6.4, 3.2, 2.5, 4.1, 26.6, 3.9, 16.1)

plt.rc('axes', labelsize= 19) 
plt.rc('xtick', labelsize= 19)    # fontsize of the tick labels
plt.rc('ytick', labelsize= 19)    # fontsize of the tick labels
plt.rc('legend', fontsize= 19)    # legend fontsize

# create plot
fig, ax = plt.subplots()
index = np.arange(n_groups)
bar_width = 0.7
opacity = 0.8
 
rects1 = plt.bar(index, times, bar_width, alpha=opacity, color=colors[0], label='Times')

#plt.xlabel('Detectors')
plt.ylabel('Time [ms]')
plt.xticks(index +
0.5*bar_width,('BRIEF','BRISK','ORB','LDB','FREAK','LATCH','BAFT', 'AKAZE'), rotation=0)

#plt.title('Processing times', size=19)
#plt.legend(loc=2) 

plt.tight_layout()
plt.show()
