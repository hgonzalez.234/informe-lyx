import numpy as np
import matplotlib.pyplot as plt
from utils.colors import colors

# data to plot
n_groups = 6

#BFM
brief = (63.9, 41.8, 70.8, 33.4, 49.11, 48.2)
brisk = (44.1, 31.8, 63, 34.4, 46.75, 34.6)
orb = (57.5, 38.6, 53.8, 20.8, 47.74, 42)
freak = (21.06, 18.6, 54.4, 21.6, 24.17, 18.4)
ldb = (61.02, 41.6, 68, 28.6, 47.74, 46)
latch = (21.06, 23.2, 17.06, 13.8, 7.07, 26.6)
baft = (37.59, 24.4, 40, 23, 40.27, 27.8)

#GMS
#brief = (71.8, 18.24, 80, 8.3, 79,2, 28.4)
#brisk = (41.43, 7.4, 82.6, 13.8, 58.07, 10.6)
#orb = (63.63, 10.4, 60, 0.2, 74.11, 20.6)
#freak = (13.8, 0.6, 59.8, 0, 29.07, 1.2)
#ldb = (65.25, 14, 72.8, 2, 74.34, 22.8)
#latch = (68.9, 23.8, 55, 2.4, 75.77, 34)
#baft = (48.3, 5, 55.2, 0, 61.7, 8.6)

plt.rc('axes', labelsize= 19) 
plt.rc('xtick', labelsize= 19)    # fontsize of the tick labels
plt.rc('ytick', labelsize= 19)    # fontsize of the tick labels
plt.rc('legend', fontsize= 19)    # legend fontsize

# create plot
fig, ax = plt.subplots()
index = np.arange(n_groups)
bar_width = 0.1
opacity = 0.8
 
rects1 = plt.bar(index, brief, bar_width, alpha=opacity, color=colors[0], label='BRIEF')
 
rects2 = plt.bar(index + bar_width, brisk, bar_width, alpha=opacity, color=colors[1], label='BRISK')

rects3 = plt.bar(index + 2*bar_width, orb, bar_width, alpha=opacity, color=colors[2], label='ORB')

rects4 = plt.bar(index + 3*bar_width, freak, bar_width, alpha=opacity, color=colors[3], label='FREAK')

rects5 = plt.bar(index + 4*bar_width, ldb, bar_width, alpha=opacity, color=colors[4], label='LDB')

rects6 = plt.bar(index + 5*bar_width, latch, bar_width, alpha=opacity, color=colors[5], label='LATCH')

rects7 = plt.bar(index + 6*bar_width, baft, bar_width, alpha=opacity, color=colors[6], label='BAFT')

plt.xlabel('Detectors')
plt.ylabel('Matched points [%]')
plt.xticks(index + 3.5*bar_width, ('FAST', 'FAST+ANMS', 'ORB', 'ORB+ANMS', 'AGAST', 'AGAST+ANMS'))

plt.title('BFM', size=19)
#plt.title('GMS', size=19)
plt.legend(loc=2, ncol=2) 

plt.tight_layout()
plt.show()
