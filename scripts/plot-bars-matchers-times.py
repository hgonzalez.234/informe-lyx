import numpy as np
import matplotlib.pyplot as plt
from utils.colors import colors

# data to plot
n_groups = 3

times = (4, 6, 31.2)

plt.rc('axes', labelsize= 19) 
plt.rc('xtick', labelsize= 19)    # fontsize of the tick labels
plt.rc('ytick', labelsize= 19)    # fontsize of the tick labels
plt.rc('legend', fontsize= 19)    # legend fontsize

# create plot
fig, ax = plt.subplots()
index = np.arange(n_groups)
bar_width = 0.7
opacity = 0.8
 
rects1 = plt.bar(index, times, bar_width, alpha=opacity, color=colors[0], label='Times')

#plt.xlabel('Detectors')
plt.ylabel('Time [ms]')
plt.xticks(index +
0.5*bar_width,('BFM','GMS','FLANN'), rotation=0)

#plt.title('Processing times', size=19)
#plt.legend(loc=2) 

plt.tight_layout()
plt.show()
